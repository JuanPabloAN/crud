'use strict'

const path = require('path');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser')
const mongoose = require('mongoose');

const app = express()


//conencting to db
mongoose.connect('mongodb://localhost/crud')
    .then(db=>console.log('db conectada'))
    .catch(err=>console.log(err))

//importing routes
const indexRoutes = require('./routes/index')



//settings
app.set('port', process.env.PORT || 3000);
app.set('views' , path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

//routes
app.use('/', indexRoutes);

//starting server

app.listen(app.get('port'), ()=>{
    //console.log(`API REST corriendo en HTTP://localhost:${app.get('port')}`)
});